﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VS.Model
{
    public class AbstractDAC : IDisposable
    {
        private OleDbConnection _connection;
        private OleDbCommand _command;

        /// <summary>
        /// Initialize connection
        /// </summary>
        private void InitConnection()
        {
            this._connection = new OleDbConnection();
            this._connection.ConnectionString = Properties.Settings.Default.strNativeConstr;
        }

        public OleDbConnection GetConnection()
        {
            return _connection;
        }

        public AbstractDAC()
        {
            this.InitConnection();
        }

        private void InitCommand()
        {
            InitCommand(string.Empty);
        }

        private void InitCommand(string query)
        {
            this._command = this._connection.CreateCommand();
            this._command.CommandText = query;
            if (this._command.Connection.State == ConnectionState.Closed)
            {
                this._command.Connection.Open();
            }
        }

        /// <summary>
        /// Retrieve record using a reader
        /// </summary>
        /// <param name="query"></param>
        /// <returns>OleDbDataReader</returns>
        public OleDbDataReader Read(string query, List<OleDbParameter> parameters = null)
        {
            try
            {
                this.InitCommand();
                using (OleDbCommand com = new OleDbCommand(query, _connection))
                {
                    com.Parameters.AddRange(parameters.ToArray());
                    OleDbDataReader reader = com.ExecuteReader();
                    return reader;
                }
            }
            catch (Exception) { }
            return null;
        }

        /// <summary>
        /// Insert, Update, Delete
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public bool ExecNonQuery(string query, List<OleDbParameter> parameters)
        {
            bool result = false;
            this.InitCommand();
            using (OleDbCommand com = new OleDbCommand(query, _connection))
            {
                com.Parameters.AddRange(parameters.ToArray());
                com.ExecuteNonQuery();
                result = true;
            }
            return result;
        }

        /// <summary>
        /// Check if the record exist
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public int? recordExist(string query, List<OleDbParameter> parameters)
        {
            try
            {
                int? result = 0;
                this.InitCommand();
                using (OleDbCommand com = new OleDbCommand(query, _connection))
                {
                    com.Parameters.AddRange(parameters.ToArray());
                    int? count = (int?)com.ExecuteScalar();
                    result = count;
                }
                return result;
            }
            catch (Exception ex)
            { }
            return null;
        }

        /// <summary>
        /// insert and return the last id inserted
        /// </summary>
        /// <param name="query"></param>
        /// <param name="identityQuery"></param>
        /// <returns></returns>
        public string InsertAndreturnID(string query, List<OleDbParameter> parameters, string identityQuery)
        {
			string ID = string.Empty;
            this.InitCommand();
            using (OleDbCommand com = new OleDbCommand(query, _connection))
            {
                com.Parameters.AddRange(parameters.ToArray());
                com.ExecuteNonQuery();
				com.CommandText = identityQuery;
				ID = com.ExecuteScalar().ToString();
            }
            return ID;
        }

        /// <summary>
        /// Get DataSet
        /// </summary>
        /// <param name="query"></param>
        /// <param name="srcTable"></param>
        /// <returns>DataSet</returns>
        public DataSet ds(string query, string srcTable)
        {
            this.InitCommand();
            using (OleDbDataAdapter da = new OleDbDataAdapter(query, _connection))
            {
                DataSet ds = new DataSet();
                da.Fill(ds, srcTable);
                return ds;
            }
        }

        /// <summary>
        /// Get DataSet
        /// </summary>
        /// <param name="query"></param>
        /// <param name="srcTable"></param>
        /// <returns>DataSet</returns>
        public DataTable dt(string query, string srcTable)
        {
            this.InitCommand();
            using (OleDbDataAdapter da = new OleDbDataAdapter(query, _connection))
            {
                DataSet ds = new DataSet();
                da.Fill(ds, srcTable);
                DataTable dt = ds.Tables[srcTable];
                return dt;
            }
        }


        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this._command != null)
                {
                    this._command.Dispose();
                }
                if (this._connection != null)
                {
                    this._connection.Dispose();
                }
            }
        }

    }
}
