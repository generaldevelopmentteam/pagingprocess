﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VS.Model;

namespace VSInventory.Model.Helpers
{
    public class PagingProcess : AbstractDAC
    {
		/// <summary>
		/// Page Index
		/// </summary>
        private int currentPageIndex = 1; 
		/// <summary>
		/// Pages to Count
		/// </summary>
        private double totalPagesCount = 0f; 
		/// <summary>
		/// Total Records
		/// </summary>
		private double totalPageRecords = 0f;

		/// <summary>
		/// Page Starts
		/// </summary>
        private double pageIndex = 0f;
		/// <summary>
		/// Page Ends
		/// </summary>
        private double maxRecord = 0f; // PageEnd
        
		/// <summary>
		/// How Many Records to page
		/// </summary>
        public int recordsToPage { get; set; } 

        public bool btnNext { get; set; }
        public bool btnPrev { get; set; }
        public bool btnLast { get; set; }
        public bool btnFirst { get; set; }
		/// <summary>
		/// Record to Start
		/// </summary>
		public int currectPageCount { get; set; }
		/// <summary>
		/// If record is in detailed mode
		/// </summary>
		public bool isSingleRec { get; set; }
        /// <summary>
        /// page records
        /// </summary>
        /// <param name="query"></param>
        /// <param name="tbl"></param>
        /// <returns></returns>
        public DataSet pagerecord(string query, string tbl)
        {
            try
            {
				if(currectPageCount >= currentPageIndex && currectPageCount != 0 && isSingleRec == true)
				{
					currentPageIndex = currectPageCount;
					pageIndex = currectPageCount - 1;
					isSingleRec = false;
				}
                using(DataSet ds = new DataSet())
                {
                    using(OleDbDataAdapter da = new OleDbDataAdapter(query, base.GetConnection()))
                    {
                        da.Fill(ds, (int)this.pageIndex, (int)this.recordsToPage, tbl); // get the first wave of records
                        this.totalPageRecords = ds.Tables[tbl].Rows.Count; //get the collected count of records
                        using(DataSet dsMC = new DataSet())
                        { // get the total count of records 
                            da.Fill(dsMC, tbl);
                            this.maxRecord = dsMC.Tables[tbl].Rows.Count;
                        }

                        this.recordsToPage = this.recordsToPage <= 0 ? (int)this.maxRecord : this.recordsToPage;
                        this.totalPagesCount = (int)(this.maxRecord / this.recordsToPage); //divide the maxrecord to the supplied record value
                        if((this.maxRecord % this.recordsToPage) > 0)
                        { // get the remainder
                            this.totalPagesCount += 1;
                        }
                        this.disablePageBtn();
                    }
                    return ds;
                }

            }
            catch(Exception ex)
            {
            }

            return null;
        }
        /// <summary>
        /// page last
        /// </summary>
        /// <returns></returns>
        public bool pageLast()
        {
            bool ret = false;
            this.pageIndex = (int)this.totalPagesCount;
            if((int)this.totalPagesCount == (int)this.pageIndex)
            {
                this.pageIndex = this.recordsToPage * (this.pageIndex - 1);
                this.currentPageIndex = (int)this.totalPagesCount;
                ret = true;
            }
            return ret;
        }
        /// <summary>
        /// page first
        /// </summary>
        /// <returns></returns>
        public bool pageFirst()
        {
            bool ret = false;
            this.pageIndex = 0;
            if(this.pageIndex == 0)
            {
                this.currentPageIndex = (this.recordsToPage * (int)(this.pageIndex)) + 1;
                this.totalPagesCount -= (int)this.totalPagesCount;
                ret = true;
            }
            return ret;
        }
        /// <summary>
        /// page next
        /// </summary>
        /// <returns></returns>
        public bool pagenext()
        {
            bool ret = false;
            if(this.pageIndex != this.totalPagesCount)
            {
                if(this.currentPageIndex >= 0) { this.currentPageIndex++; }
                ret = true;
            }
            this.pageIndex += this.recordsToPage; // sends the paged count to the start
            return ret;
        }
        /// <summary>
        /// page previous
        /// </summary>
        /// <returns></returns>
        public bool pagePrev()
        {
            bool ret = false;
            if(this.currentPageIndex >= 0)
            {
                if(this.currentPageIndex > 0) { this.currentPageIndex--; }
                ret = true;
            }
            this.pageIndex -= this.recordsToPage;// sends the paged count to the start
            return ret;
        }
        /// <summary>
        /// displays info
        /// </summary>
        /// <returns></returns>
        public string displayInfo()
        {
            string message = String.Format("Page - {0} / {1} - {2}",
                                            (int)this.currentPageIndex, // current page
                                            (int)this.totalPagesCount, // total page
                                            (int)this.totalPageRecords + " Records"); // total records
            return message;
        }
		/// <summary>
		/// If record is in detail mode
		/// </summary>
		/// <returns></returns>
		public string displayOnePageInfo()
		{
			string message = String.Format(" {0} / {1}", // current page
											this.totalPagesCount.ToString(),
											this.currentPageIndex.ToString()); // total records
			return message;
		}
        /// <summary>
        /// disables the button
        /// </summary>
        private void disablePageBtn()
        {
            if((int)this.totalPagesCount == this.currentPageIndex)
            {
                this.btnNext = false;
                this.btnLast = false;
            }
            else
            {
                this.btnNext = true;
                this.btnLast = true;
            }
            if(this.currentPageIndex <= 1)
            {
                this.btnPrev = false;
                this.btnFirst = false;
            }
            else
            {
                this.btnPrev = true;
                this.btnFirst = true;
            }
        }
    }
}
